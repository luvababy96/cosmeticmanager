package com.lsw.cosmeticmanager.controller;

import com.lsw.cosmeticmanager.model.CosmeticRequest;
import com.lsw.cosmeticmanager.service.CosmeticService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/cosmetic")
public class CosmeticController {

    private final CosmeticService cosmeticService;

    @PostMapping("/data")
    public String setCosmetic(@RequestBody CosmeticRequest request){
     cosmeticService.setCosmetic(request.getType(),request.getBrand(), request.getOwner());
     return "OK";
    }

}

package com.lsw.cosmeticmanager.service;

import com.lsw.cosmeticmanager.entity.Cosmetic;
import com.lsw.cosmeticmanager.repository.CosmeticRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class CosmeticService {

    private final CosmeticRepository cosmeticRepository;

    public void setCosmetic(String type, String brand, String owner) {
        Cosmetic addData = new Cosmetic();
        addData.setType(type);
        addData.setBrand(brand);
        addData.setOwner(owner);

        cosmeticRepository.save(addData);
    }
}

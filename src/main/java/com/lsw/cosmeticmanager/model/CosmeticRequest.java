package com.lsw.cosmeticmanager.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CosmeticRequest {

    private String type;

    private String brand;

    private String owner;

}

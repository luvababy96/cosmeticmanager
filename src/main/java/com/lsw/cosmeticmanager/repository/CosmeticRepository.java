package com.lsw.cosmeticmanager.repository;

import com.lsw.cosmeticmanager.entity.Cosmetic;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CosmeticRepository extends JpaRepository<Cosmetic, Long> {
}
